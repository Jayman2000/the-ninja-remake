# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends TextureRect


const FIRST_LEVEL = "res://scenes_and_scripts/levels/1.tscn"


func _unhandled_input(event):
	if event.is_action_pressed("ui_accept"):
		var scene_changer = get_node("/root/SceneChanger")
		scene_changer.try_to_change_scene(FIRST_LEVEL)
