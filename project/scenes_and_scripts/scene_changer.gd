# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends Node


signal error(error_code, message)
const FATAL_ERROR_SCENE = "res://scenes_and_scripts/error/fatal.tscn"
const TEMPLATE = 'ERROR: Failed to change to this scene: "%s".'


func change_scene_log_errors(path):
	var error_code = get_tree().change_scene(path)
	var message

	if error_code == OK:
		message = null
	else:
		message = TEMPLATE % [path]
		printerr(message, " Error code: %s." % error_code)

	return [error_code, message]


func try_to_change_scene(path, fallback=FATAL_ERROR_SCENE):
	var main_result = change_scene_log_errors(path)

	if main_result[0] != OK:
		var fallback_result = change_scene_log_errors(fallback)

		if fallback_result[0] == OK:
			var tree = get_tree()
			yield(tree, "node_added")  # Wait for current_scene to change
			yield(tree.current_scene, "ready")  # Wait for connections
			emit_signal("error", main_result[0], main_result[1])
		else:
			get_tree().quit(fallback_result[0])
