# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends Panel


const TEMPLATE = "WARNING: Fatal error message (probably) won't be displayed. Error code: %s."

func _ready():
	var scene_changer = get_node("/root/SceneChanger")
	var error_code = scene_changer.connect(
			"error",
			self,
			"_on_SceneChanger_error")

	if error_code != OK:
		printerr(TEMPLATE % [error_code])


func _on_Button_pressed():
	get_tree().quit()


func _on_SceneChanger_error(error_code, message):
	$VSplitContainer/Message.text = message + "\nError code: %s" % [error_code]
