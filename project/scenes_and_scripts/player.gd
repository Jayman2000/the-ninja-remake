# SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
# SPDX-License-Identifier: CC0-1.0

extends KinematicBody2D


const MAX_HORIZONTAL_ACCELERATION = 2000  # Unit: px/sec^2
# Unit: portion of speed that's left over
const HORIZONTAL_FRICTION_COEFFICENT = 0.9
const GRAVITY_ACCELERATION = 800  # Unit: px/sec^2
const JUMP_ACCELERATION = -45000  # Unit: px/sec^2

onready var velocity = Vector2(0, 0)  # Units: x=px/sec, y=px/sec


func _physics_process(delta):
	# Unit: x=px/sec^2, y=px/sec^2
	var acceleration = Vector2(0, GRAVITY_ACCELERATION)

	if Input.is_action_pressed("ui_left"):
		acceleration.x -= Input.get_action_strength("ui_left")
	if Input.is_action_pressed("ui_right"):
		acceleration.x += Input.get_action_strength("ui_right")
	acceleration.x *= MAX_HORIZONTAL_ACCELERATION

	if is_on_floor() and Input.is_action_just_pressed("ui_accept"):
		acceleration.y += JUMP_ACCELERATION

	# The units for acceleration are px/sec^2, and the units for velocity are
	# px/sec. As a result, we have to multipy acceleration by delta to convert
	# it into px/sec.
	velocity += acceleration * delta
	velocity.x *= HORIZONTAL_FRICTION_COEFFICENT

	velocity = move_and_slide(velocity, Vector2.UP)
