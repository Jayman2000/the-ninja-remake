<!--SPDX-FileCopyrightText: 2021 Jason Yundt <swagfortress@gmail.com>
SPDX-License-Identifier: CC0-1.0
-->
# The Ninja Remake
It's a remake of [The Ninja](https://scratch.mit.edu/projects/22620682/) by [Will_Wam](https://scratch.mit.edu/users/Will_Wam).

## Copying
This repo should be [REUSE](https://reuse.software/) compliant. For full copying information, install the [reuse tool](https://github.com/fsfe/reuse-tool), and run
`$ reuse spdx`

## Running from source
1. Make sure that [Git](https://git-scm.com/) and [Godot](https://godotengine.org/) are installed.
1. Clone this repo: `$ git clone https://gitlab.com/Jayman2000/the-ninja-remake.git`
1. (Optional) If you're going to contribute to this Project, then you may want to have your contributions automatically checked. To do so,

	1. Make sure that [pre-commit](https://pre-commit.com/) is installed.
	1. `cd` to the root of the repo.
	1. Run `$ pre-commit install`

1. Open Godot.
1. Click "Import".
1. Select the "project/project.godot" file from the cloned repo.
1. Click "Import & Edit".
1. Press F5.
